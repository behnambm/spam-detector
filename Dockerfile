FROM python:3.8-slim

RUN mkdir -p /app
WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

RUN echo "import nltk; nltk.download('stopwords'); nltk.download('wordnet')" | python


COPY static/ static/
COPY templates/ templates/
COPY ./svc_mod.pkl svc_mod.pkl
COPY ./tfidf.pkl tfidf.pkl 

COPY main.py main.py


ENV DEBUG=False

EXPOSE 5000

ENTRYPOINT [ "python", "main.py"]


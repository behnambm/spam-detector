import re
from os.path import dirname, join, realpath

import joblib
import nltk
from flask import Flask, render_template, request
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

with open(join(dirname(realpath(__file__)), "svc_mod.pkl"), "rb") as f:
    model = joblib.load(f)

with open(join(dirname(realpath(__file__)), "tfidf.pkl"), "rb") as f:
    vc = joblib.load(f)


lemmatizer = WordNetLemmatizer()



def preprocess(text):
    message = re.sub('[^a-zA-Z]', ' ', text)
    message = message.lower()
    message = message.split()
    message = [lemmatizer.lemmatize(word) for word in message if not word in stopwords.words('english')]
    return ' '.join(message)


app = Flask(__name__)


@app.get("/")
def index():
    return render_template('index.html')


@app.post("/")
def predict():
    data = request.form
    if data.get("text"):
        clean_text = preprocess(data.get("text"))
        vectorized_text = vc.transform([clean_text]).toarray()

        prediction = model.predict(vectorized_text)
        
        prediction = int(prediction[0])
        target_class = None
        if prediction == 0:
            target_class = "spam"
        if prediction == 1:
            target_class = "normal"

    return render_template('index.html', prediction=target_class)


if __name__ == '__main__':
    # nltk.download('stopwords')
    # nltk.download('wordnet')
    
    app.run(host="0.0.0.0")


